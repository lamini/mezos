all: 3

4:
	TEZOS_PROTO=4 dune build @install

3:
	TEZOS_PROTO=3 dune build @install

2:
	TEZOS_PROTO=2 dune build @install

1:
	TEZOS_PROTO=1 dune build @install

alpha:
	TEZOS_PROTO=0 dune build @install

clean:
	dune clean
