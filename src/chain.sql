create table chain (
  hash char(15) primary key
);

create table block (
  hash char(51) unique,
  level int,
  proto int not null,
  predecessor char(51) not null,
  timestamp timestamp not null,
  validation_passes smallint not null,
  merkle_root char(53) not null,
  fitness varchar(64) not null,
  context_hash char(52) not null,
  primary key (hash, level)
);

create table operation (
  hash char(51) primary key,
  chain char(15) not null,
  blk char(51) not null,
  foreign key (chain) references chain (hash),
  foreign key (blk) references block (hash)
);
create index operation_chain on operation (chain);
create index operation_blk on operation (blk);

create table operation_alpha (
  hash char(51) not null,
  id smallint not null,
  cat smallint not null,
  primary key (hash, id),
  foreign key (hash) references operation (hash)
);
create index operation_alpha_cat on operation_alpha (cat);

create table implicit (
  pkh char(36) primary key,
  activated char(51),
  revealed char(51),
  pk varchar(55),
  foreign key (activated) references block (hash),
  foreign key (revealed)  references block (hash)
);
create index implicit_activated on implicit (activated);
create index implicit_revealed on implicit (revealed);

create table endorsement (
  blk char(51),
  op char(51),
  id smallint,
  level int,
  pkh char(36),
  slot smallint,
  primary key (blk, op, id, level, pkh, slot),
  foreign key (blk) references block (hash),
  foreign key (op)  references operation (hash),
  foreign key (pkh) references implicit (pkh)
);

create table block_alpha (
  hash char(51) primary key,
  baker char(36) not null,
  level_position int not null,
  cycle int not null,
  cycle_position int not null,
  voting_period int not null,
  voting_period_position int not null,
  voting_period_kind smallint not null,
  consumed_gas varchar(64) not null,
  foreign key (hash) references block (hash),
  foreign key (baker) references implicit (pkh)
);
create index block_alpha_baker on block_alpha (baker);
create index block_alpha_level_position on block_alpha (level_position);
create index block_alpha_cycle on block_alpha (cycle);
create index block_alpha_cycle_position on block_alpha (cycle_position);

create table deactivated (
  pkh char(36) not null,
  blk char(51) not null,
  primary key (pkh, blk),
  foreign key (pkh) references implicit (pkh),
  foreign key (blk) references block (hash)
);

create table contract (
  k char(36) primary key,
  blk char(51) not null,
  mgr char(36),
  delegate char(36),
  spendable bool not null,
  delegatable bool not null,
  credit bigint,
  preorig char(36),
  script text,
  foreign key (blk) references block (hash),
  foreign key (mgr) references implicit (pkh),
  foreign key (delegate) references implicit (pkh),
  foreign key (preorig) references contract (k)
);
create index contract_blk on contract (blk);
create index contract_mgr on contract (mgr);
create index contract_delegate on contract (delegate);
create index contract_preorig on contract (preorig);

create table tx (
  op char(51) not null,
  op_id smallint not null,
  source char(36) not null,
  destination char(36) not null,
  fee bigint not null,
  amount bigint not null,
  parameters text,
  primary key (op, op_id),
  foreign key (op, op_id) references operation_alpha (hash, id),
  foreign key (source) references contract (k),
  foreign key (destination) references contract (k)
);
create index tx_source on tx (source);
create index tx_destination on tx (destination);

create table origination (
  op char(51) not null,
  op_id smallint not null,
  source char(36) not null,
  k char(36) not null,
  primary key (op, op_id),
  foreign key (op, op_id) references operation_alpha (hash, id),
  foreign key (source) references contract (k),
  foreign key (k) references contract (k)
);
create index origination_source on origination (source);
create index origination_k on origination (k);

create table delegation (
  op char(51) not null,
  op_id smallint not null,
  source char(36) not null,
  pkh char(36),
  primary key (op, op_id),
  foreign key (op, op_id) references operation_alpha (hash, id),
  foreign key (source) references contract (k),
  foreign key (pkh) references implicit (pkh)
);
create index delegation_source on delegation (source);
create index delegation_pkh on delegation (pkh);

create table balance (
  blk char(51) not null,
  op char(51),
  op_id smallint,
  cat smallint not null,
  k char(36) not null,
  cycle int,
  diff bigint not null,
  foreign key (blk) references block (hash),
  foreign key (op, op_id) references operation_alpha (hash, id),
  foreign key (k) references contract (k)
);
create index balance_blk   on balance (blk);
create index balance_op    on balance (op, op_id);
create index balance_cat   on balance (cat);
create index balance_k     on balance (k);
create index balance_cycle on balance (cycle);

create table snapshot (
  cycle int,
  level int,
  primary key (cycle, level)
);

create table delegate (
  cycle int not null,
  level int not null,
  pkh char(36) not null,
  balance bigint not null,
  frozen_balance bigint not null,
  staking_balance bigint not null,
  delegated_balance bigint not null,
  deactivated bool not null,
  grace smallint not null,
  primary key (cycle, pkh),
  foreign key (cycle, level) references snapshot (cycle, level),
  foreign key (pkh) references implicit (pkh)
);

create table delegated_contract (
  delegate char(36),
  delegator char(36),
  cycle int,
  level int,
  primary key (delegate, delegator, cycle, level),
  foreign key (delegate) references implicit (pkh),
  foreign key (delegator) references contract (k),
  foreign key (cycle, level) references snapshot (cycle, level)
);
create index delegated_contract_cycle on delegated_contract (cycle);
create index delegated_contract_level on delegated_contract (level);

create table stake (
  delegate char(36) not null,
  level int not null,
  k char(36) not null,
  kind smallint not null,
  diff bigint not null,
  primary key (delegate, level, k, kind, diff),
  foreign key (delegate) references implicit (pkh),
  foreign key (k) references contract (k)
);

-- VIEWS

create view level as
  select level, hash from block order by level asc;

create view tx_full as
  select op, op_id,
  b.hash as blk,
  b.level as level,
  b.timestamp as timestamp,
  source, k1.mgr as source_mgr,
  destination, k2.mgr as destination_mgr,
  fee, amount, parameters
  from tx
  join operation on tx.op = operation.hash
  join block b on operation.blk = b.hash
  join contract k1 on tx.source = k1.k
  join contract k2 on tx.destination = k2.k;

create view balance_full as
  select block.level, block_alpha.cycle,
  cycle_position, op, op_id, k, cat, diff
  from block
  natural join block_alpha
  join balance on block.hash = balance.blk;
