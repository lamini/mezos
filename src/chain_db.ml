(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Caqti_type.Std
open Caqti_request
open Tezos_sql

module AC = Alpha_context

let src = Logs.Src.create "mezos.chain_db"

let count_operation_alpha =
  create_p oph int Caqti_mult.one
    (fun _di -> "select count() from operation_alpha where hash = ?")

let nb_alpha_operations conn oph =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.find count_operation_alpha oph >>=
  Caqti_lwt.or_fail

let update_snapshot =
  create_p (tup2 int int32) unit Caqti_mult.zero
    (fun _di -> "insert into snapshot values (?, ?)")

let select_snapshots =
  create_p unit (tup2 int int32) Caqti_mult.zero_or_more
    (fun _di -> "select * from snapshot")

let store_snapshot_levels conn levels =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  with_transaction conn update_snapshot levels

let snapshot_levels conn =
  let open Alpha_context in
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.fold select_snapshots begin fun (cycle, level) a ->
    Cycle.Map.add (Obj.magic cycle) level a
  end () Cycle.Map.empty >>=
  Caqti_lwt.or_fail

module Stake_table = struct
  type t = {
    delegate: Signature.Public_key_hash.t ;
    level: int32 ;
    contract: Alpha_context.Contract.t ;
    kind: [`Contract | `Rewards] ;
    amount: int64 ;
  }

  let kind =
    let decode = function
      | 0 -> Ok `Contract
      | 1 -> Ok `Rewards
      | _ -> Error "state_kind" in
    custom
      ~encode:(function `Contract -> Ok 0 | `Rewards -> Ok 1)
      ~decode int

  let sql_encoding =
    custom
      ~encode:(fun { delegate ; level ; contract ; kind ; amount } ->
          Ok ((delegate, level), (contract, kind, amount)))
      ~decode:(fun ((delegate, level), (contract, kind, amount)) ->
          Ok { delegate ; level ; contract ; kind ; amount })
      (tup2 (tup2 pkh int32) (tup3 k kind int64))

  let insert =
    create_p sql_encoding unit Caqti_mult.zero begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into stake values (?, ?, ?, ?, ?)"
      | `Mysql -> "insert ignore into stake values (?, ?, ?, ?, ?)"
      | `Pgsql -> "insert into stake values (?, ?, ?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

  let select_by_delegate_and_max_level =
    create_p (tup2 pkh int32) sql_encoding Caqti_mult.zero_or_more
      (fun _di -> "select * from stake where delegate = ? and level <= ?")

  let select_by_delegate_and_k =
    create_p (tup2 pkh k) sql_encoding Caqti_mult.zero_or_more
      (fun _di -> "select * from stake where delegate = ? and k = ?")
end

open Alpha_context

type tx_full = {
  op_hash : Operation_hash.t ;
  id : int ;
  blk_hash : Block_hash.t ;
  level : int ;
  timestamp : Time.Protocol.t ;
  src : Contract.t ;
  src_mgr : Signature.public_key_hash option ;
  dst : Contract.t ;
  dst_mgr : Signature.public_key_hash option ;
  fee : Tez.t ;
  amount : Tez.t ;
  parameters : Script.lazy_expr option ;
}

let tx_full_sql_encoding =
  let encode { op_hash; id; blk_hash; level;
               timestamp; src; src_mgr; dst; dst_mgr;
               fee; amount; parameters } =
    Ok ((op_hash, id, blk_hash, level),
        (timestamp, src, src_mgr, dst),
        (dst_mgr, fee, amount, parameters)) in
  let decode ((op_hash, id, blk_hash, level),
              (timestamp, src, src_mgr, dst),
              (dst_mgr, fee, amount, parameters)) =
    Ok { op_hash; id; blk_hash; level;
         timestamp; src; src_mgr; dst; dst_mgr;
         fee; amount; parameters } in
  custom ~encode ~decode
    (tup3
       (tup4 oph int blk_hash int)
       (tup4 time k (option pkh) k)
       (tup4 (option pkh) tez tez (option lazy_expr)))

let tx_full_encoding =
  let open Data_encoding in
  conv
    (fun { op_hash; id; blk_hash; level; timestamp; src; src_mgr;
           dst; dst_mgr ; amount; fee; parameters } ->
      ((op_hash, id), (blk_hash, level, timestamp, src, src_mgr,
                       dst, dst_mgr, amount, fee, parameters)))
    (fun ((op_hash, id), (blk_hash, level, timestamp, src, src_mgr,
                          dst, dst_mgr, amount, fee, parameters)) ->
      { op_hash; id; blk_hash; level; timestamp; src; src_mgr;
        dst; dst_mgr; amount; fee; parameters })
    (merge_objs
       (obj2
          (req "op_hash" Operation_hash.encoding)
          (req "id" uint16))
       (obj10
          (req "blk_hash" Block_hash.encoding)
          (req "level" int31)
          (req "timestamp" Time.Protocol.encoding)
          (req "src" Contract.encoding)
          (opt "src_mgr" Signature.Public_key_hash.encoding)
          (req "dst" Contract.encoding)
          (opt "dst_mgr" Signature.Public_key_hash.encoding)
          (req "amount" Tez.encoding)
          (req "fee" Tez.encoding)
          (opt "parameters" Script.lazy_expr_encoding)))

let select_tx_full =
  create_p (tup2 k k) tx_full_sql_encoding
    Caqti_mult.zero_or_more
    (fun _di -> "select * from tx_full where source = ? or destination = ?")

module IntMap = Map.Make(struct
    type t = int
    let compare = Pervasives.compare end)

let create_tx_from_db tx a =
  Operation_hash.Map.update tx.op_hash begin function
    | None -> Some (IntMap.singleton tx.id tx)
    | Some txmap ->
      Some (IntMap.add tx.id tx txmap)
  end a

let find_txs_involving_k conn k =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.fold select_tx_full create_tx_from_db
    (k, k) Operation_hash.Map.empty >>=
  Caqti_lwt.or_fail

module Balance_update = struct
  type t = (Delegate.balance * Delegate.balance_update) list

  type table = {
    blk_hash: Block_hash.t ;
    op: (Operation_hash.t * int) option ;
    balance: Delegate.balance ;
    k: Contract.t option ;
    cycle: Cycle.t option ;
    diff: Delegate.balance_update ;
  }

  let table =
    custom
      ~encode:begin fun { blk_hash ; op ;
                          balance ; k ; cycle ; diff } ->
        let op_hash, op_id =
          match op with
          | Some (op_hash, op_id) -> Some op_hash, Some op_id
          | None -> None, None in
        Ok ((blk_hash, op_hash, op_id, balance), (k, cycle, diff))
      end
      ~decode:begin fun ((blk_hash, op_hash, op_id, balance),
                         (k, cycle, diff)) ->
        let op =
          match op_hash, op_id with
          | Some op_hash, Some op_id -> Some (op_hash, op_id)
          | _ -> None in
        Ok { blk_hash ; op ; balance ; k ; cycle ; diff }
      end
      (tup2
         (tup4 blk_hash (option oph) (option int) balance)
         (tup3 (option k) (option cycle) balance_update))

  let update_balance =
    create_p table unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into balance values (?, ?, ?, ?, ?, ?, ?)"
        | `Mysql -> "insert ignore into balance values (?, ?, ?, ?, ?, ?, ?)"
        | `Pgsql -> "insert into balance values (?, ?, ?, ?, ?, ?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

  let update_tables ?op conn ~blk_hash (t: t) =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    with_transaction conn update_balance @@
    List.map begin fun (balance, diff) ->
      match balance with
      | Delegate.Contract k ->
        { k = Some k ; blk_hash ; op ;
          balance ; cycle = None ; diff }
      | Rewards (pkh, cycle)
      | Fees (pkh, cycle)
      | Deposits (pkh, cycle) ->
        let k = Contract.implicit_contract pkh in
        { k = Some k ; blk_hash ; op ;
          balance ; cycle = Some cycle; diff }
    end t
end

type tx = {
  op : Operation_hash.t ;
  op_id : int ;
  source : Contract.t ;
  destination : Contract.t ;
  fee : Tez.t ;
  amount : Tez.t ;
  parameters : Script.lazy_expr option ;
}

let tx =
  custom
    ~encode:begin fun { op ; op_id ; source ; destination ;
                        fee ; amount ; parameters } ->
      Ok ((op, op_id, source, destination),
          (fee, amount, parameters))
    end
    ~decode:begin fun ((op, op_id, source, destination),
                       (fee, amount, parameters)) ->
      Ok { op ; op_id ; source ; destination ;
           fee ; amount ; parameters }
    end
    (tup2 (tup4 oph int k k) (tup3 tez tez (option lazy_expr)))

let insert_tx =
  create_p tx unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into tx values (?, ?, ?, ?, ?, ?, ?)"
      | `Mysql -> "insert ignore tx values (?, ?, ?, ?, ?, ?, ?)"
      | `Pgsql -> "insert into tx values (?, ?, ?, ?, ?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

module Contract_table = struct
  type t = {
    k: Contract.t ;
    blk_hash : Block_hash.t ;
    manager: Signature.Public_key_hash.t ;
    delegate: Signature.Public_key_hash.t option ;
    spendable: bool ;
    delegatable: bool ;
    credit: Tez.t option ;
    preorigination: Contract.t option ;
    script: Script.t option ;
  }

  let sql_encoding =
    custom
      ~encode:begin fun { k ; blk_hash ; manager ; delegate ;
                          spendable ; delegatable ;
                          credit ; preorigination ; script } ->
        Ok ((k, blk_hash, manager),
            (delegate, spendable, delegatable),
            (credit, preorigination, script))
      end
      ~decode:begin
        fun ((k, blk_hash, manager),
             (delegate, spendable, delegatable),
             (credit, preorigination, script)) ->
          Ok { k ; blk_hash ; manager ; delegate ;
               spendable ; delegatable ;
               credit ; preorigination ; script }
      end
      (tup3
         (tup3 k blk_hash pkh)
         (tup3 (option pkh) bool bool)
         (tup3 (option tez) (option k) (option script)))

  let insert =
    create_p sql_encoding unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into contract values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        | `Mysql -> "insert ignore contract values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        | `Pgsql -> "insert into contract values (?, ?, ?, ?, ?, ?, ?, ?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

  let update_delegate =
    create_p (tup2 (option pkh) k) unit Caqti_mult.zero
      (fun _di -> "update contract set delegate = ? where k = ?")

  let json_encoding =
    let open Data_encoding in
    conv
      (fun { blk_hash ; manager ; delegate ; spendable ; delegatable ; script } ->
         (blk_hash, manager, delegate, spendable, delegatable, script))
      (fun _ -> invalid_arg "Contract_repr.json_encoding")
      (obj6
         (req "blk" Block_hash.encoding)
         (req "mgr" Signature.Public_key_hash.encoding)
         (opt "delegate" Signature.Public_key_hash.encoding)
         (req "spendable" bool)
         (req "delegatable" bool)
         (opt "script" Script.encoding))

  let select =
    create_p unit sql_encoding Caqti_mult.zero_or_one
      (fun _di -> "select * from contract order by k")

  let select_by_k =
    create_p k sql_encoding Caqti_mult.zero_or_one
      (fun _di -> "select * from contract where k = ?")

  let select_by_mgr =
    create_p pkh sql_encoding Caqti_mult.zero_or_more
      (fun _di -> "select * from contract where mgr = ?")

  let insert_discovered =
    create_p (tup2 (tup3 k blk_hash (option pkh)) (tup2 bool bool)) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into contract (k, blk, mgr, spendable, delegatable) values (?, ?, ?, ?, ?)"
        | `Mysql -> "insert ignore into contract (k, blk, mgr, spendable, delegatable) values (?, ?, ?, ?, ?)"
        | `Pgsql -> "insert into contract (k, blk, mgr, spendable, delegatable) values (?, ?, ?, ?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
end

module Implicit_table = struct
  let insert_discovered =
    create_p pkh unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into implicit (pkh) values (?)"
        | `Mysql -> "insert ignore into implicit (pkh) values (?)"
        | `Pgsql -> "insert into implicit (pkh) values (?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end

  let upsert_reveal =
    create_p (tup3 k (tup2 blk_hash pk) (tup2 blk_hash pk)) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or replace into implicit (pkh, revealed, \
                      pk) values (?, ?, ?)"
        | `Mysql -> "insert into implicit (pkh, revealed, pk) values \
                     (?, ?, ?) on duplicate key update revealed = ?, \
                     pk = ?"
        | `Pgsql -> "insert into implicit (pkh, revealed, pk) values \
                     (?, ?, ?) on conflict (pkh) do update set revealed = ?, \
                     pk = ?"
        | _ -> invalid_arg "not implemented"
      end

  let upsert_activated =
    create_p (tup3 pkh blk_hash blk_hash) unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or replace into implicit (pkh, activated) values (?, ?)"
        | `Mysql -> "insert into implicit (pkh, activated) values (?, \
                     ?) on duplicate key update activated = ?"
        | `Pgsql -> "insert into implicit (pkh, activated) values (?, \
                     ?) on conflict (pkh) do update set activated = ?"
        | _ -> invalid_arg "not implemented"
      end
end

module Origination_table = struct
  type t = {
    op: Operation_hash.t ;
    op_id: int ;
    src: Contract.t ;
    k: Contract.t ;
  }

  let encoding =
    custom
      ~encode:(fun { op ; op_id ; src ; k } -> Ok (op, op_id, src, k))
      ~decode:(fun (op, op_id, src, k) -> Ok { op ; op_id ; src ; k })
      (tup4 oph int k k)

  let insert =
    create_p encoding unit Caqti_mult.zero
      begin fun di ->
        match Caqti_driver_info.dialect_tag di with
        | `Sqlite -> "insert or ignore into origination values (?, ?, ?, ?)"
        | `Mysql -> "insert ignore into origination values (?, ?, ?, ?)"
        | `Pgsql -> "insert into origination values (?, ?, ?, ?) on conflict do nothing"
        | _ -> invalid_arg "not implemented"
      end
end

let insert_delegation =
  create_p (tup4 oph int k (option pkh)) unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into delegation values (?, ?, ?, ?)"
      | `Mysql -> "insert ignore into delegation values (?, ?, ?, ?)"
      | `Pgsql -> "insert into delegation values (?, ?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let update_contract conn k blk_hash =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  begin match Contract.is_implicit k with
    | None -> (* originated *)
      Conn.exec Contract_table.insert_discovered ((k, blk_hash, None), (true, true)) >>=
      Caqti_lwt.or_fail
    | Some pkh -> (* implicit account *)
      Conn.exec Implicit_table.insert_discovered pkh >>=
      Caqti_lwt.or_fail >>= fun () ->
      Conn.exec Contract_table.insert_discovered ((k, blk_hash, Some pkh), (true, false)) >>=
      Caqti_lwt.or_fail
  end >>= fun () ->
  return_unit

let process_mgr_operation :
  type a b.
  (module Caqti_lwt.CONNECTION) ->
  Block_hash.t -> Operation_hash.t -> int -> Contract.t -> Tez.t ->
  a manager_operation ->
  b Apply_results.successful_manager_operation_result -> unit tzresult Lwt.t =
  fun conn blk_hash op_hash i source fee operation operation_result ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let process_tx amount destination balance_updates originated_contracts parameters =
    (* Add discovered contracts *)
    with_transaction
      conn Contract_table.insert_discovered
      (List.map (fun k -> (k, blk_hash, None), (true, true)) originated_contracts) >>= fun () ->
    (* Add src and dst *)
    update_contract conn source blk_hash >>=? fun () ->
    update_contract conn destination blk_hash >>=? fun () ->
    (* Update balances *)
    Balance_update.update_tables
      ~op:(op_hash, i) conn ~blk_hash balance_updates >>= fun () ->
    (* Update tx *)
    Conn.exec insert_tx
      { op = op_hash ; op_id = i ; source ; destination ;
        fee ; amount ; parameters } >>=
    Caqti_lwt.or_fail >>= fun () ->
    Logs_lwt.debug ~src begin fun m ->
      m "Stored TX %a (%d)" Operation_hash.pp op_hash i
    end >>= fun () ->
    return_unit in
  let process_origination
      manager delegate spendable delegatable
      credit preorigination script balance_updates originated_contracts =
    assert (List.length originated_contracts = 1) ;
    let originated_contract = List.hd originated_contracts in
    (* Add discovered implicits *)
    Conn.exec Implicit_table.insert_discovered manager >>=
    Caqti_lwt.or_fail >>= fun () ->
    begin match delegate with
      | None -> Lwt.return_unit
      | Some delegate ->
        Conn.exec Implicit_table.insert_discovered delegate >>=
        Caqti_lwt.or_fail
    end >>= fun () ->
    (* Insert full contract row *)
    Conn.exec Contract_table.insert
      { k = originated_contract ; blk_hash ; manager ; delegate ; spendable ;
        delegatable ; credit = Some credit ; preorigination ; script } >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* Insert in origination table *)
    Conn.exec Origination_table.insert {
      op = op_hash ; op_id = i ; src = source ;
      k = originated_contract} >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* Update balances *)
    Balance_update.update_tables
      ~op:(op_hash, i) conn ~blk_hash balance_updates >>= fun () ->
    Logs_lwt.debug ~src begin fun m ->
      m "Stored origination of %a" Contract.pp source
    end >>= fun () ->
    return_unit in
  match operation, operation_result with
  | Transaction { amount ; destination ; parameters },
    Apply_results.Transaction_result
      { balance_updates ; originated_contracts } ->
    process_tx amount destination
      balance_updates originated_contracts parameters
  | Reveal pk, _ ->
    Conn.exec Implicit_table.upsert_reveal (source, (blk_hash, pk), (blk_hash, pk)) >>=
    Caqti_lwt.or_fail >>= fun () ->
    Logs_lwt.debug ~src begin fun m ->
      m "Stored revelation of %a" Signature.Public_key.pp pk
    end >>= fun () ->
    return_unit
  | Origination { manager; delegate; spendable;
                  delegatable; credit; preorigination ; script },
    Apply_results.Origination_result { balance_updates ;
                                       originated_contracts } ->
    process_origination
      manager delegate spendable delegatable
      credit preorigination script balance_updates originated_contracts
  | Delegation maybe_pkh, _ ->
    Conn.exec insert_delegation (op_hash, i, source, maybe_pkh) >>=
    Caqti_lwt.or_fail >>= fun () ->
    Conn.exec Contract_table.update_delegate (maybe_pkh, source) >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* Logs_lwt.debug ~src begin fun m ->
     *   match maybe_pkh with
     *   | None -> m "Stored remove delegation %a" Contract.pp source
     *   | Some pkh -> m "Stored add delegation %a -> %a"
     *                 Contract.pp source Signature.Public_key_hash.pp pkh
     * end >>= fun () -> *)
    return_unit
  | _, _ -> assert false

let int_of_contents : type a. a contents -> int = function
  | Endorsement _ -> 0
  | Seed_nonce_revelation _ -> 1
  | Double_endorsement_evidence _ -> 2
  | Double_baking_evidence _ -> 3
  | Activate_account _ -> 4
  | Proposals _ -> 5
  | Ballot _ -> 6
  | Manager_operation { operation; _ } ->
    match operation with
    | Reveal _ -> 7
    | Transaction _ -> 8
    | Origination _ -> 9
    | Delegation _ -> 10

let insert_operation_alpha =
  create_p (tup3 oph int int) unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into operation_alpha values (?, ?, ?)"
      | `Mysql -> "insert ignore into operation_alpha values (?, ?, ?)"
      | `Pgsql -> "insert into operation_alpha values (?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let store_op_id :
  type a b.
  (module Caqti_lwt.CONNECTION) ->
  Operation_hash.t -> int -> Block_hash.t ->
  a contents -> b Apply_results.contents_result -> unit tzresult Lwt.t =
  fun conn op_hash i blk_hash contents meta ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.exec insert_operation_alpha
    (op_hash, i, int_of_contents contents) >>=
  Caqti_lwt.or_fail >>= fun () ->
  match contents, meta with
  | Seed_nonce_revelation _,
    Apply_results.Seed_nonce_revelation_result balance_updates ->
    Balance_update.update_tables conn ~blk_hash ~op:(op_hash, i)
      balance_updates >>= fun () ->
    return_unit
  | Double_endorsement_evidence _,
    Apply_results.Double_endorsement_evidence_result balance_updates ->
    Balance_update.update_tables conn ~blk_hash ~op:(op_hash, i)
      balance_updates >>= fun () ->
    return_unit
  | Double_baking_evidence _,
    Apply_results.Double_baking_evidence_result balance_updates ->
    Balance_update.update_tables conn ~blk_hash ~op:(op_hash, i)
      balance_updates >>= fun () ->
    return_unit
  | Activate_account { id; _ },
    Apply_results.Activate_account_result balance_updates ->
    (* Add implicit *)
    Conn.exec
      Implicit_table.upsert_activated (Ed25519 id, blk_hash, blk_hash) >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* Add contract *)
    Conn.exec Contract_table.insert_discovered
      ((Contract.implicit_contract (Ed25519 id), blk_hash, None), (true, false)) >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* Update balances *)
    Balance_update.update_tables conn ~blk_hash ~op:(op_hash, i)
      balance_updates >>= fun () ->
    return_unit
  | Endorsement { level = _ },
    Apply_results.Endorsement_result { balance_updates ;
                                       delegate = _ ;
                                       slots = _ } ->
    (* Update endorsements *)
    (* Lwt_list.iter_s begin fun slot ->
     *   Sqlexpr.execute db update_endorsement_stmt
     *     blk_hash_str op_hash_str i
     *     (Raw_level.to_int32 level)
     *     (Signature.Public_key_hash.to_b58check delegate) slot
     * end slots >>= fun () -> *)
    (* Update balances *)
    Balance_update.update_tables ~op:(op_hash, i)
      conn ~blk_hash balance_updates >>= fun () ->
    return_unit
  | Manager_operation { source; fee; operation; _ },
    Apply_results.Manager_operation_result {
      balance_updates ; (* Correspond to the operation fees *)
      operation_result = Applied operation_result; (* matched and processed later *)
      internal_operation_results ; (* matched and processed later *)
    } ->
    let process_internal_op_results a =
      iter_s begin function
        | Apply_results.Internal_operation_result
            ({ source; operation; nonce = _ }, Applied operation_result) ->
          process_mgr_operation conn blk_hash op_hash i
            source fee operation operation_result
        | _ ->
          (* Do not process effects of failed operations *)
          return_unit
      end a in
    process_internal_op_results internal_operation_results >>=? fun () ->
    process_mgr_operation conn blk_hash op_hash i
      source fee operation operation_result >>=? fun () ->
    (* Update balances (global operation fee) *)
    Balance_update.update_tables conn ~blk_hash ~op:(op_hash, i)
      balance_updates >>= fun () ->
    return_unit
  | Manager_operation _, _ ->
    (* failed/skipped/backtracked manager operation, ignore. *)
    return_unit
  | _, _ ->
    assert false

let insert_operation =
  create_p (tup3 oph chain_id blk_hash) unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into operation values (?, ?, ?)"
      | `Mysql -> "insert ignore into operation values (?, ?, ?)"
      | `Pgsql -> "insert into operation values (?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let store_op conn blk_hash
    ({ Alpha_block_services.chain_id ; hash = op_hash ;
       protocol_data = Operation_data { contents } ; receipt }) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let rec inner :
    type a. int -> a contents_list ->
    operation_receipt -> unit tzresult Lwt.t =
    fun i contents receipt -> match contents, receipt with
      | Single _, Apply_results.No_operation_metadata ->
        assert false
      | Cons _, Apply_results.No_operation_metadata ->
        assert false
      | Single _, Apply_results.Operation_metadata { contents = Apply_results.Cons_result _ } ->
        assert false
      | Cons _, Apply_results.Operation_metadata { contents = Apply_results.Single_result _} ->
        assert false
      | Single op,
        Apply_results.Operation_metadata { contents = Apply_results.Single_result a } ->
        store_op_id conn op_hash i blk_hash op a
      | Cons (op, rest),
        Apply_results.Operation_metadata { contents = Apply_results.Cons_result (a, meta)} ->
        store_op_id conn op_hash i blk_hash op a >>=? fun () ->
        inner (succ i) rest (Apply_results.Operation_metadata { contents = meta })
  in
  Conn.exec insert_operation (op_hash, chain_id, blk_hash) >>=
  Caqti_lwt.or_fail >>= fun () ->
  Logs_lwt.debug ~src begin fun m ->
    m "Stored raw operation %a" Operation_hash.pp op_hash
  end >>= fun () ->
  inner 0 contents receipt

let insert_shell_header =
  create_p (tup2 blk_hash shell_header) unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into block values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
      | `Mysql -> "insert ignore into block values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
      | `Pgsql -> "insert into block values (?, ?, ?, ?, ?, ?, ?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let insert_deactivated =
  create_p (tup2 pkh blk_hash) unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into deactivated values (?, ?)"
      | `Mysql -> "insert ignore into deactivated values (?, ?)"
      | `Pgsql -> "insert into deactivated values (?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let insert_blk_hdr_meta =
  create_p
    (tup3
       (tup4 blk_hash pkh int32 cycle)
       (tup4 int32 voting_period int32 voting_period_kind) z)
    unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into block_alpha values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
      | `Mysql -> "insert ignore into block_alpha values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
      | `Pgsql -> "insert into block_alpha values (?, ?, ?, ?, ?, ?, ?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let insert_chain =
  create_p chain_id unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into chain values (?)"
      | `Mysql -> "insert ignore into chain values (?)"
      | `Pgsql -> "insert into chain values (?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let store_blk conn blk_hash
    ({ shell ; _ } : Alpha_block_services.raw_block_header)
    ({ protocol_data =
         { baker; level = { level_position;
                            cycle; cycle_position;
                            voting_period;
                            voting_period_position; _ };
           voting_period_kind;
           nonce_hash = _;
           consumed_gas ;
           deactivated ;
           balance_updates };
       test_chain_status = _; _ } : Alpha_block_services.block_metadata) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  (* Store shell header *)
  Conn.exec insert_shell_header (blk_hash, shell) >>=
  Caqti_lwt.or_fail >>= fun () ->
  (* Discover deactivated pkhs and fill up deactivated table *)
  with_transaction conn Implicit_table.insert_discovered deactivated >>= fun () ->
  with_transaction conn insert_deactivated
    (List.map (fun pkh -> pkh, blk_hash) deactivated) >>= fun () ->
  (* Store alpha header *)
  Conn.exec insert_blk_hdr_meta
    ((blk_hash, baker, level_position, cycle),
     (cycle_position, voting_period, voting_period_position, voting_period_kind),
     consumed_gas) >>=
  Caqti_lwt.or_fail >>= fun () ->
  (* Update balances tables *)
  Balance_update.update_tables conn ~blk_hash balance_updates >>= fun () ->
  return_unit

let discover_initial_ks cctxt blockid conn =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Alpha_block_services.info cctxt
    ~chain:(fst blockid) ~block:(snd blockid) () >>=?
  fun { chain_id; hash; header = { shell ; _ }; _ } ->
  Alpha_services.Contract.list cctxt blockid >>=? fun ks ->
  iter_s begin fun k ->
    Alpha_services.Contract.info
      cctxt blockid k >>=? fun { manager; spendable; _ } ->
    (* add implicit *)
    Conn.exec Implicit_table.insert_discovered manager >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* add chain *)
    Conn.exec insert_chain chain_id >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* add shell header *)
    Conn.exec insert_shell_header (hash, shell) >>=
    Caqti_lwt.or_fail >>= fun () ->
    (* add contract *)
    let delegatable = match Contract.is_implicit k with
      | Some _ -> false
      | None -> true in
    Conn.exec Contract_table.insert_discovered
      ((k, hash, Some manager), (spendable, delegatable)) >>=
    Caqti_lwt.or_fail >>= fun () ->
    Logs_lwt.debug ~src
      (fun m -> m "Added contract %a" Contract.pp k) >>= fun () ->
    return_unit
  end ks >>=? fun _ ->
  return_unit

let select_max_level =
  create_p unit (option int32) Caqti_mult.one
    (fun _di -> "select max(level) from block")

let bootstrap_generic
    ?(blocks_per_sql_tx=300l) ?from ?up_to ~first_alpha_level cctxt conn
    ~(f:#full ->
      (module Caqti_lwt.CONNECTION) ->
      int32 -> unit tzresult Lwt.t) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.find select_max_level () >>=
  Caqti_lwt.or_fail >>= fun level ->
  let from =
    match from, level with
    | Some lvl, _ -> lvl
    | None, None -> first_alpha_level
    | None, Some lvl -> Int32.succ lvl in
  begin match level with
    | Some _ -> return_unit
    | None -> discover_initial_ks cctxt (`Main, `Level first_alpha_level) conn
  end >>=? fun () ->
  Logs_lwt.info ~src begin fun m ->
    m "Start downloading chain from level %ld" from
  end >>= fun () ->
  let rec process_n_blks conn counter lvl =
    match counter, up_to with
    | c, _ when c <= 0l -> return (`Counter_exhausted, lvl)
    | _, Some target when lvl > target ->
      Logs.debug ~src begin fun m ->
        m "Reached target level %ld, bootstrapping done." target
      end ;
      return (`Target_reached, lvl)
    | _ ->
      Logs.debug ~src (fun m -> m "Processing block level %ld" lvl) ;
      protect begin fun () ->
        f cctxt conn lvl >>=? fun () ->
        return `Reached
      end ~on_error:begin function
        | [RPC_context.Not_found _] ->
          return `Reached_head
        | e -> Lwt.return (Error e)
      end >>=? function
      | `Reached ->
        process_n_blks conn (Int32.pred counter) (Int32.succ lvl)
      | `Reached_head ->
        return (`Reached_head, lvl)
  in
  let rec inner lvl =
    let blocks_to_download =
      match up_to with
      | None -> blocks_per_sql_tx
      | Some up_to ->
        min (Int32.sub up_to lvl) blocks_per_sql_tx in
    if blocks_to_download <= 0l then begin
      Logs.debug ~src (fun m -> m "Bootstrapping done") ;
      return lvl
    end else
      Conn.start () >>=
      Caqti_lwt.or_fail >>= fun () ->
      process_n_blks conn blocks_to_download lvl >>=? fun (status, lvl) ->
      Conn.commit () >>=
      Caqti_lwt.or_fail >>= fun () ->
      match status with
      | `Reached_head -> return lvl
      | `Target_reached -> return lvl
      | `Counter_exhausted -> inner lvl
  in
  inner from

let store_ops db blk_hash ops =
  match ops with
  | [endorsements; _voting; anon; manager] ->
    iter_s (store_op db blk_hash) endorsements >>=? fun () ->
    iter_s (store_op db blk_hash) anon >>=? fun () ->
    iter_s (store_op db blk_hash) manager
  | _ -> assert false

let store_blk_full ?chain ?block cctxt conn =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Alpha_block_services.info cctxt ?chain ?block () >>=? fun
    { chain_id ;
      hash ;
      header = { shell ; _ } as header ;
      metadata ;
      operations } ->
  (* Store chain *)
  Conn.exec insert_chain chain_id >>=
  Caqti_lwt.or_fail >>= fun () ->
  (* Store block header (shell + alpha) *)
  store_blk conn hash header metadata >>=? fun () ->
  (* Store ops (shell + alpha) *)
  store_ops conn hash operations >>=? fun () ->
  Logs_lwt.info ~src begin fun m ->
    m "Stored %a (level: %ld)" Block_hash.pp_short hash shell.level
  end >>= fun () ->
  return_unit

let bootstrap_chain ?blocks_per_sql_tx ?from ?up_to ~first_alpha_level cctxt db =
  bootstrap_generic ?blocks_per_sql_tx ?from ?up_to ~first_alpha_level cctxt db
    ~f:begin fun cctxt db lvl ->
      store_blk_full ~block:(`Level lvl) cctxt db
    end

let insert_delegated_contract =
  create_p (tup4 pkh kh cycle int32) unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into delegated_contract values (?, ?, ?, ?)"
      | `Mysql -> "insert ignore into delegated_contract values (?, ?, ?, ?)"
      | `Pgsql -> "insert into delegated_contract values (?, ?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let insert_delegate =
  create_p delegate_info unit Caqti_mult.zero
    begin fun di ->
      match Caqti_driver_info.dialect_tag di with
      | `Sqlite -> "insert or ignore into delegate values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
      | `Mysql -> "insert ignore into delegate values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
      | `Pgsql -> "insert into delegate values (?, ?, ?, ?, ?, ?, ?, ?, ?) on conflict do nothing"
      | _ -> invalid_arg "not implemented"
    end

let select_delegate =
  create_p pkh delegate_info Caqti_mult.zero_or_more
    (fun _di -> "select * from delegate where pkh = ? order by cycle")

let get_delegate_info_at_snap cctxt conn delegate (cycle, level) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Delegate_services.info cctxt (`Main, `Level level) delegate >>= function
  | Error err ->
    Logs_lwt.err begin fun m ->
      m "At %ld (cycle %a):@ %a" level Cycle.pp cycle pp_print_error err
    end
  | Ok d ->
    Logs_lwt.info begin fun m ->
      let open Data_encoding.Json in
      m "Got delegate info for %ld (cycle %a):@ %a" level Cycle.pp cycle pp
        (construct Delegate_services.info_encoding d)
    end >>= fun () ->
    with_transaction conn insert_delegated_contract
      (List.map begin fun k -> (delegate, k, cycle, level)
       end d.delegated_contracts) >>= fun () ->
    Conn.exec insert_delegate { Tezos_sql.cycle; level; pkh = delegate ; info = d } >>=
    Caqti_lwt.or_fail

let bootstrap_delegate cctxt db delegate =
  snapshot_levels db >>= fun snaps ->
  let snaps = Cycle.Map.bindings snaps in
  Lwt_list.iter_s
    (get_delegate_info_at_snap cctxt db delegate) snaps >>= fun () ->
  return_unit

let history_one ?(display=false) conn k =
  find_txs_involving_k conn k >>= fun txs ->
  Logs.app ~src begin fun m ->
    m "Found %d transactions involving %a"
      (Operation_hash.Map.cardinal txs)
      Alpha_context.Contract.pp k
  end ;
  if display then begin
    Operation_hash.Map.iter begin fun _op_hash txs ->
      IntMap.iter begin fun _op_id tx ->
        let tx_json = Data_encoding.Json.construct tx_full_encoding tx in
        Logs.app ~src (fun m -> m "%a" Data_encoding.Json.pp tx_json)
      end txs
    end txs
  end ;
  return txs

let history ?display chain_db =
  map_s (history_one ?display chain_db)

type balance_full = {
  level: int32 ;
  cycle: Cycle.t ;
  cycle_position: int32 ;
  op: (Operation_hash.t * int) option ;
  cat : Delegate.balance ;
  diff : Tez.t ;
}

let balance_full =
  custom
    ~encode:begin fun { level ; cycle ;
                        cycle_position ; op ; cat ; diff } ->
      let op, op_id =
        match op with
        | None -> None, None
        | Some (op, id) -> Some op, Some id in
      Ok ((level, cycle, cycle_position),
          (op, op_id, cat, diff))
    end
    ~decode:begin fun ((level, cycle, cycle_position),
                       (op, op_id, cat, diff)) ->
      let op = match op, op_id with
        | Some op, Some op_id -> Some (op, op_id)
        | _ -> None in
      Ok { level ; cycle ; cycle_position ; op ; cat ; diff }
    end
    (tup2
       (tup3 int32 cycle int32)
       (tup4 (option oph) (option int) balance tez))

let select_balance_full =
  create_p (tup3 k int32 int32) balance_full Caqti_mult.zero_or_more
    (fun _di -> "select * from balance_full where k = ? and level >= ? \
                 and level <= ? order by level")

