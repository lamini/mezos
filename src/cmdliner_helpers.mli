(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cmdliner

module Convs : sig
  val uri : Uri.t Arg.conv
end

module Terms : sig
  val setup_log : unit Term.t
  val tezos_client_dir : string Term.t
  val datadir : string Term.t
  val force : doc:string -> bool Term.t
  val cfg_file : string Term.t

  val host :
    ?argnames:string list ->
    default:string -> doc:string -> unit -> string Term.t
  val port :
    ?argnames:string list ->
    default:int -> doc:string -> unit -> int Term.t

  val uri :
    default:Uri.t -> doc:string -> args:string list ->
    Uri.t Term.t
  val uri_option :
    ?default:Uri.t -> doc:string -> args:string list ->
    unit -> Uri.t option Term.t

  val db : default:Uri.t -> Uri.t Term.t

  val ovh_log : (Uri.t * string) option Term.t
  val tls : bool Term.t

  val alias_pos : int -> string Term.t
  val alias_opt : string option Term.t
end
