(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cmdliner

let src = Logs.Src.create "mezos.bootstrap"

let default_db_url =
  Uri.make ~scheme:"postgresql" ~host:"localhost" ~path:"chain" ()

let snapshot_levels db () =
  Tezos_sql.connect db >>= fun conn ->
  let open Tzscan in
  V3.snapshot_levels () >>= function
  | `Ok (Some lvls) ->
    let lvls = List.sort Int32.compare lvls in
    let lvls = List.mapi (fun i l -> i+7, l) lvls in
    Chain_db.store_snapshot_levels conn lvls >>= fun () ->
    return_unit
  | #RPC.service_result ->
    failwith "error while querying TZScan"

let bootstrap_delegate tezos_url tezos_client_dir db delegate () =
  Tezos_sql.connect db >>= fun conn ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt =
    new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  Chain_db.bootstrap_delegate cctxt conn delegate

let bootstrap
    first_alpha_level from up_to blocks_per_sql_tx
    tezos_url tezos_client_dir db () =
  Tezos_sql.connect db >>= fun conn ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt =
    new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  Chain_db.bootstrap_chain ~first_alpha_level
    cctxt ?blocks_per_sql_tx ?from ?up_to conn >>=? fun reached ->
  Logs_lwt.app ~src begin fun m ->
    m "Reached level %ld, exiting." reached
  end >>= fun () ->
  return_unit

(* let load_balances_cmd =
 *   let open Cmdliner_helpers.Terms in
 *   let from = Arg.(value & opt int32 2l &
 *                   info ["from"] ~doc:"Which block level") in
 *   let doc = "Load first Alpha block and store it in SQLite." in
 *   Term.(const load_balances $
 *         uri ~args:["tezos-url"] ~doc:"URL of a running Tezos node" $
 *         tezos_client_dir $
 *         datadir $ from $ setup_log),
 *   Term.info ~doc "load-balances" *)

let discover =
  let doc = "Specify level of first alpha block" in
  Arg.(value & opt int32 2l & info ~doc ["first-block"])

let bootstrap_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "Download Tezos and store it in DB." in
  let from = Arg.(value & opt (some int32) None &
                  info ["from"] ~doc:"Where to start") in
  let up_to = Arg.(value & opt (some int32) None &
                   info ["upto"] ~doc:"Where to stop") in
  let blocks_per_sql_tx = Arg.(value & opt (some int32) None &
                               info ["save-increment"]
                                 ~doc:"How often to synchronize on disk") in
  Term.(const bootstrap $ discover $ from $ up_to $ blocks_per_sql_tx $
        uri_option ~args:["tezos-url"] ~doc:"URL of a running Tezos node" () $
        tezos_client_dir $
        db ~default:default_db_url $
        setup_log),
  Term.info ~doc "all"

let bootstrap_delegate_cmd =
  let open Cmdliner_helpers.Terms in
  let open Old_alpha_cmdliner_helpers.Convs in
  let doc = "Download delegate information in DB." in
  let delegate =
    Arg.(required & pos 0 (some address) None & info [] ~docv:"PKH") in
  Term.(const bootstrap_delegate $
        uri_option ~args:["tezos-url"] ~doc:"URL of a running Tezos node" () $
        tezos_client_dir $
        db ~default:default_db_url $
        delegate $
        setup_log),
  Term.info ~doc "delegate"

let snapshot_levels_cmd =
  let open Cmdliner_helpers.Terms in
  let open Old_alpha_cmdliner_helpers.Convs in
  let doc = "Get snapshot levels from TZScan and store in DB." in
  Term.(const snapshot_levels $
        db ~default:default_db_url $
        setup_log),
  Term.info ~doc "snapshot-levels"

let build_stake_db db delegate related start_cycle end_cycle () =
  Tezos_sql.connect db >>= fun conn ->
  Mezos_stake.build_stake_db conn
    ~related ?start_cycle ?end_cycle delegate >>= fun _ ->
  return_unit

let build_stake_db_cmd =
  let open Cmdliner_helpers.Terms in
  let open Old_alpha_cmdliner_helpers.Convs in
  let doc = "Build the stake DB." in
  let delegate =
    Arg.(required & pos 0 (some address) None & info [] ~docv:"PKH") in
  let related =
    Arg.(value & pos_right 0 contract [] & info [] ~docv:"KS") in
  let start_cycle =
    Arg.(value & opt (some int32) None &
         info ["s" ; "start-cycle"] ~doc:"First cycle to process") in
  let end_cycle =
    Arg.(value & opt (some int32) None &
         info ["e" ; "end-cycle"] ~doc:"Max cycle to process") in
  Term.(const build_stake_db $
        db ~default:default_db_url $
        delegate $
        related $
        start_cycle $
        end_cycle $
        setup_log),
  Term.info ~doc "build-stake-db"

let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Logs.err ~src (fun m -> m "%a" pp_exn exn) ;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Logs.err ~src (fun m -> m "%a" pp_print_error err) ;
    exit 1
  | Ok () -> ()

let cmds =
  List.map begin fun (term, info) ->
    Term.((const lwt_run) $ term), info
  end [
    build_stake_db_cmd ;
    snapshot_levels_cmd ;
    bootstrap_cmd ;
    bootstrap_delegate_cmd ;
  ]

let default_cmd =
  let doc = "Download Tezos and store it in DB." in
  Term.(ret (const (`Help (`Pager, None)))),
  Term.info ~doc "mezos-bootstrap"

let () = match Term.eval_choice default_cmd cmds with
  | `Error _ -> exit 1
  | #Term.result -> exit 0
