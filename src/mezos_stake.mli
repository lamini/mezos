(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Alpha_context

module KMap : Map.S with type key = Contract.t
module Int32Map : Map.S with type key = int32

type diff = {
  amount : int64 ;
  kind : [`Contract | `Rewards] ;
}

val diff_encoding : diff Data_encoding.t

type fulldiff = {
  level : int32 ;
  diff : diff ;
}

val create_fulldiff :
  level:int32 ->
  kind:[`Contract | `Rewards] ->
  amount:int64 -> fulldiff

val fulldiff_encoding : fulldiff Data_encoding.t

type t = (int64 * (diff list)) Int32Map.t KMap.t

val empty : t

val recover_stake_db :
  ?max_level:int32 ->
  (module Caqti_lwt.CONNECTION) ->
  public_key_hash -> t Lwt.t

val build_stake_db :
  (module Caqti_lwt.CONNECTION) ->
  ?related:Contract.t list ->
  ?start_cycle:int32 ->
  ?end_cycle:int32 ->
  public_key_hash -> t Lwt.t

